import java.io.InputStream;
import java.sql.*;
import java.util.Scanner;
public class CoursesOperations {
	
		public static void printCourses1(Connection connection)throws SQLException {
			System.out.println("Creating Statement...");
			Statement stmt=connection.createStatement();
			String sqlQuery="SELECT * FROM student";
			ResultSet queryResult=stmt.executeQuery(sqlQuery);
			
			while (queryResult.next()) {
				int id=queryResult.getInt("course_id");
				String name=queryResult.getString("name");
				String profesor = queryResult.getString("profesor");
				String studyYear = queryResult.getString("study_year");
				
				System.out.println("ID: " + id + "Name: " + name + "profesor: " + profesor + "stuyYear: " +studyYear);
			}
			queryResult.close();
			stmt.close();
		}
		public static void insert(Courses c) throws SQLException {
			String statement= "INSERT INTO courses (nume, profesor, studyYear) VALUES (?, ?, ?)";
			PreparedStatement prepSt = DBConnection.getConnection().prepareStatement(statement);
			prepSt.setString(1, c.getNume());
			prepSt.setString(2, c.getProfesor());
			prepSt.setInt(3, c.getStudyYear());
			
			int succes= prepSt.executeUpdate();
			if(succes == 1)
				System.out.println("Course inserted!");
		}
		
		
		public static Courses readStudentValues() {
			Courses cu = new Courses();
			
			Scanner scanner = Scanner(System.in);
			boolean quit = false;
			int index = 0;
			System.out.println("Choose\n" + "1 to enter a new student \n" + "0 to quit");
			
		while (!quit) {
			System.out.println("Choose an option: ");
			int choice = scanner.nextInt();
			scanner.nextLine();
			switch (choice) {
			case 0:
			quit = true;
			break;
			case 1:
			System.out.println	("Enter course name:");
			String name = scanner.nextLine();
			cu.setNume(name);
			System.out.println ("Enter teacher's name:");
			String profesor = scanner.nextLine();
			cu.setProfesor(profesor);
			System.out.println ("Enter study year:");
			int studyYear = scanner.nextInt();
			cu.setStudyYear(studyYear);
			break;
			}
		}
		return cu;
		}
		
		public Courses getCourseById(Connection connection, int CourseID) throws SQLException {
			Courses foundCourse= new Courses();
			String sqlQuery="SELECT CourseID, Nume, Profesor, StudyYear FROM Courses";
			PreparedStatement prepSt= connection.prepareStatement(sqlQuery);
			prepSt.setInt(100, CourseID);
			ResultSet queryResult=prepSt.executeQuery();
			
			while (queryResult.next()) {
				int id=queryResult.getInt("CourseID");
				String Nume=queryResult.getString("Nume");
				String Profesor=queryResult.getString("Teacher");
				int StudyYear=queryResult.getInt("StudyYear");
				
				foundCourse.setCourseID(id);
				foundCourse.setNume(Nume);
				foundCourse.setProfesor(Profesor);
				foundCourse.setStudyYear(StudyYear);
				
				System.out.println("Course ID: "+id+", Nume: "+Nume+", Profesor: "+Profesor+", An:"+StudyYear);
			}
			return foundCourse;
		}
		
		public static void delete(int idToDelete) throws SQLException{
			
			
			try {
			String statement= "DELETE from courses where CourseID=?";
			PreparedStatement prepSt=DBConnection.getConnection().prepareStatement(statement);
			prepSt.setInt(1, idToDelete);


			int succes = prepSt.executeUpdate();
			
			if(succes == 1)
				System.out.println("Course deleted!");
			} catch (SQLException e)
			{
				System.out.println("Error! Check data!");
			}
		}
		
		public static void update(Courses c) 
		{
			try {
				String statement="UPDATE courses SET Nume=?, Profesor=?, StudyYear=? where CourseID=?";
				PreparedStatement prepSt=DBConnection.getConnection().prepareStatement(statement);
				prepSt.setString(1, c.getNume());
				prepSt.setString(2, c.getProfesor());
				prepSt.setInt(4, c.getCourseID());
				prepSt.setInt(3, c.getStudyYear());
				int succes = prepSt.executeUpdate();
				
				if(succes == 1)
					System.out.println("Course Updated!");
				} catch (SQLException e)
				{
					System.out.println("Error! Check data!");
				}
				
			}
		
		public static void viewCourses() {
			try { 
				PreparedStatement pst = DBConnection.getConnection().prepareStatement("Select * FROM Courses");
				ResultSet rs = pst.executeQuery();
				System.out.println("-------------------------------------------------------------------------------------------------------------");
				while(rs.next()) {
				System.out.printf("|ID: %s | Name: %s | Teacher: %s | StudyYear: %s |\n", rs.getInt("CourseID"), rs.getString("Nume"), rs.getString("profesor"), rs.getString("studyYear"));
				}

			} catch (SQLException e) {
				System.out.println("Error! Check data!");
			}
		}
		
		public static Courses readCoursesValues1() {
			Courses cu=new Courses();
			
			Scanner scanner=new Scanner(System.in);
			
			return cu;
		}
		
		private static Scanner Scanner(InputStream in) {
			return new Scanner(in);
		}
		public static void printCourses(Connection connection) {
			// TODO Auto-generated method stub
}
		
		public static void main(String[] args) {
			
		}
}