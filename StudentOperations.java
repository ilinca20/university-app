import java.io.InputStream;
import java.sql.*;
import java.util.Scanner;

public class StudentOperations {
		
			public static void printStudents(Object connection)throws SQLException {
				System.out.println("Creating Statement...");
				Statement stmt=((Connection) connection).createStatement();
				String sqlQuery="SELECT * FROM student";
				ResultSet queryResult=stmt.executeQuery(sqlQuery);
				
				while (queryResult.next()) {
					int id=queryResult.getInt("studentID");
					String name=queryResult.getString("Nume");
					String birthDate = queryResult.getString("BirthDate");
					String  address= queryResult.getString("Address");
					
					System.out.println("ID: " + id + " name: " + name + " birthDate: " + birthDate + " address: " +address);
				}
				queryResult.close();
				stmt.close();
			}
			public static void insertStudent(Student s) throws SQLException {
				String statement= "INSERT INTO student (Nume, BirthDate, Address)VALUES (?, ?, ?)";
				PreparedStatement prepSt = DBConnection.getConnection().prepareStatement(statement);
				prepSt.setString(1, s.getNume());
				prepSt.setString(2, s.getBirthDate());
				prepSt.setString(3, s.getAddress());
				
				int succes = prepSt.executeUpdate();
				
				if(succes == 1)
					System.out.println("Student inserted!");
			}
			
			public static Student readStudentValues() {
				Student st = new Student();
				/*	
				Scanner scanner = Scanner(System.in);
				boolean quit = false;
				int index = 0;
				System.out.println("Choose\n" + "1 to enter a new student \n" + "0 to quit");
				
			while (!quit) {
				System.out.println("Choose an option: ");
				int choice = scanner.nextInt();
				scanner.nextLine();
				switch (choice) {
				case 0:
				quit = true;
				break;
				case 1:
				System.out.println	("Enter student's name:");
				String name = scanner.nextLine();
				st.setNume(name);
				System.out.println ("Enter student's birthdate:");
				String birthDate = scanner.nextLine();
				st.setBirthDate(birthDate);
				System.out.println ("Enter study year:");
				String address = scanner.nextLine();
				st.setAddress(address);
				break;
				}
				*/
			return st;
			}
			
			public Student getStudentById(Connection connection, int StudentID) throws SQLException {
				Student foundStudent= new Student();
				String sqlQuery="SELECT StudentID, Nume, BirthDate, Address FROM Student";
				PreparedStatement prepSt= connection.prepareStatement(sqlQuery);
				prepSt.setInt(100, StudentID);
				ResultSet queryResult=prepSt.executeQuery();
				
				while (queryResult.next()) {
					int id=queryResult.getInt("StudentID");
					String nume=queryResult.getString("Nume");
					String birthDate=queryResult.getString("BirthDate");
					String address=queryResult.getString("Address");
					
					foundStudent.setStudentID(id);
					foundStudent.setNume(nume);
					foundStudent.setBirthDate(birthDate);
					foundStudent.setAddress(address);
					
					System.out.println("Student ID: "+id+", Nume: "+nume+", birthdate: "+birthDate+", address:"+address);
				}
				return foundStudent;
			}
			
			public static void deleteStudent(int idToDelete) throws SQLException{
				String statement= "DELETE from student where StudentID=?;";
				PreparedStatement prepSt=DBConnection.getConnection().prepareStatement(statement);
				prepSt.setInt(1, idToDelete);
				prepSt.executeUpdate();
			}
			
			public static void updateStudent(Student s) 
				{
				
				try {
					String statement="UPDATE student SET Nume=?, BirthDate=?, Address=? where StudentID=?;";
					PreparedStatement prepSt=DBConnection.getConnection().prepareStatement(statement);
					prepSt.setString(1, s.getNume());
					prepSt.setString(2, s.getBirthDate());
					prepSt.setString(3, s.getAddress());
					prepSt.setInt(4, s.getStudentID());
					int succes = prepSt.executeUpdate();
					
					if(succes == 1)
						System.out.println("Student Updated!");
					} catch (SQLException e)
					{
						System.out.println("Error! Check data!");
					}
					
			
				}
			public static void viewStudents() {
				try { 
					PreparedStatement pst = DBConnection.getConnection().prepareStatement("Select * FROM Student");
					ResultSet rs = pst.executeQuery();
					System.out.println("-------------------------------------------------------------------------------------------------------------");
					while(rs.next()) {
					System.out.printf("|ID: %s | Name: %s | Birthday: %s | Adresa: %s |\n", rs.getInt("StudentID"), rs.getString("Nume"), rs.getString("birthdate"), rs.getString("address"));
					}

				} catch (SQLException e) {
					System.out.println("Error! Check data!");
				}
			}
			
}