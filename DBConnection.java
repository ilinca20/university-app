import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	
	private static final Connection NULL = null;
	private static String Server="DESKTOP-7Q4AKNG";
	private static String port="1433";
	private static String user="sa";
	private static String password="ilinca";
	private static String database="University";
	private static String jdbcurl;
	//private static final String DRIVER_NAME="com.mysql.jdbc.Driver";
	
	static Connection con=NULL;
	
	static {
		
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch 
		(ClassNotFoundException e) {
			e.printStackTrace();
		}
		jdbcurl="jdbc:sqlserver://"+Server+":"+port+"; user="+user+"; password="+password+";databasename="+database+"";
		
		try {
			con=DriverManager.getConnection(jdbcurl);
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static Connection getConnection() throws SQLException  {
		con=DriverManager.getConnection(jdbcurl);
		return con;
		}

	//public Connection connection;
	}