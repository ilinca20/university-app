public class Courses {

	private int CourseID;
	private String Nume;
	private String Profesor;
	private int StudyYear;
	
	

	public int getCourseID() {
		return CourseID;
	}



	public void setCourseID(int courseID) {
		CourseID = courseID;
	}



	public String getNume() {
		return Nume;
	}



	public void setNume(String nume) {
		Nume = nume;
	}



	public String getProfesor() {
		return Profesor;
	}



	public void setProfesor(String profesor) {
		Profesor = profesor;
	}



	public int getStudyYear() {
		return StudyYear;
	}



	public void setStudyYear(int studyYear) {
		StudyYear = studyYear;
	}
	
	public Courses() {
		
	}
	
	public Courses(int courseID, String nume, String profesor, int studyYear) {
		CourseID=courseID;
		Nume=nume;
		Profesor=profesor;
		StudyYear=studyYear;
	}
	
	public Courses(String nume, String profesor, int studyYear) {
		Nume=nume;
		Profesor=profesor;
		StudyYear=studyYear;
	}
	
	public String toString() {
		return "CourseID: "+CourseID+", nume: "+Nume+", profesor: "+Profesor+", studyYear: "+StudyYear;
	}
	
	public static void main(String[] args) {
		
	}
}
