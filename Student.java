

public class Student {
	
	private int StudentID;
	private String Nume;
	private String BirthDate;
	private String Address;

	public int getStudentID() {
		return StudentID;
	}



	public void setStudentID(int studentID) {
		StudentID = studentID;
	}



	public String getNume() {
		return Nume;
	}



	public void setNume(String nume) {
		Nume = nume;
	}



	public String getBirthDate() {
		return BirthDate;
	}



	public void setBirthDate(String birthDate) {
		BirthDate = birthDate;
	}



	public String getAddress() {
		return Address;
	}



	public void setAddress(String address) {
		Address = address;
	}


	public Student() {
		
	}

	public Student(int studentID, String nume, String birthDate, String address) {
		super();
		StudentID = studentID;
		Nume = nume;
		BirthDate = birthDate;
		Address = address;
	}


	public Student(String nume, String birthDate, String address) {
		
		this.Nume=nume;
		this.BirthDate=birthDate;
		this.Address=address;
	}
	
		
		public String toString() {
			
			return "StudentID=" +StudentID+ ", name="+ Nume+", birthday: " +BirthDate+", address: "+Address;            
		}
		
		public static void main(String[] args) {
			
		}
}



