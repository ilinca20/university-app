import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EnrolmentOperations {

public static void enrollStudentToCourseId( Student s, Courses c)throws SQLException {
		try {
			String statement = "INSERT INTO enrolment (StudentID, CourseID) VALUES (?,?)";
			PreparedStatement prepSt = DBConnection.getConnection().prepareStatement(statement);
			prepSt.setInt(1, s.getStudentID());
			prepSt.setInt(2, c.getCourseID());
			prepSt.executeUpdate();
		} catch(SQLException e)
		{
			System.out.println("Error! Check data!");
		}
}

	public static void viewEnrolment() {
		try { 
			PreparedStatement pst = DBConnection.getConnection().prepareStatement("Select * FROM Enrolment");
			ResultSet rs = pst.executeQuery();
			System.out.println("-------------------------------------------------------------------------------------------------------------");
			while(rs.next()) {
				System.out.printf("|StudentID: %s | CourseID: %s\n", rs.getInt("StudentID"), rs.getInt("CourseID"));
		}

	} catch (SQLException e) {
		System.out.println("Error! Check data!");
	}
}

}

