public class Enrolment {

	private int StudentID;
	private int CourseID;
	
	public Enrolment(int studentID, int courseID) {
		this.StudentID = studentID;
		this.CourseID = courseID;
	}
	
	public Enrolment() {
		
	}
	
	public int getStudentID() {
		return StudentID;
	}

	public void setStudentID(int studentID) {
		StudentID = studentID;
	}

	public int getCourseID() {
		return CourseID;
	}

	public void setCourseID(int courseID) {
		CourseID = courseID;
	}
	
public static void main(String[] args) {
		
	}
}